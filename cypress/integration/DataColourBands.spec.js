const testData = require('../../example_api_response.json');

describe('Viewing the root page', () => {
  beforeEach(() => {
    cy.server();
    cy.route({
      method: 'GET',
      response: testData,
      url: 'https://api.carbonintensity.org.uk/generation',
    });

    cy.visit('/');
  });

  it('should load the page', () => {
    cy.queryByText('Coding Example').should('exist');
  });

  it('should show all the  fuels in order of percentage', () => {
    const sortedFuels = testData.data.generationmix
      .sort((a, b) => a.perc - b.perc)
      .reverse();

    cy.queryAllByTestId('components/ColourDataStream').each(
      (stream, index) => {
        const { fuel, perc } = sortedFuels[index];

        expect(stream.text()).to.equal(`${fuel} (${perc}%)`);
}
);
});
});
