import React, { ComponentType } from 'react';
import PropTypes, { InferProps } from 'prop-types';
import styled from '@emotion/styled';
import { color, flexbox, layout, space, typography } from 'styled-system';

interface StyleColourDataStreamProps {
  alignContent: string;
  bg: string;
  display: string;
  fontSize: string;
  height: number;
  justifyContent: string;
  p: number;
}

const ColouredDataStream: ComponentType<
  StyleColourDataStreamProps
> = styled.div(color, flexbox, layout, space, typography);

const ColourDataStreamProps = {
  color: PropTypes.string,
  fuel: PropTypes.string,
  percentage: PropTypes.number,
};

const ColourDataStream = ({
  color,
  fuel,
  percentage,
}: InferProps<typeof ColourDataStreamProps>) => (
  <ColouredDataStream
    alignContent="center"
    bg={color}
    data-testid="components/ColourDataStream"
    display="flex"
    fontSize={`${percentage > 2 ? 1 * (percentage / 5) : 0.5}rem`}
    justifyContent="center"
    p={4}
    height={percentage * 5}
  >
    {fuel} ({percentage}%)
  </ColouredDataStream>
);

ColourDataStream.propTypes = ColourDataStreamProps;

export default ColourDataStream;
