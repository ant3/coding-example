import React from 'react';
import { render } from '@testing-library/react';
import ColourDataBands from '../index';

import testData from '../../../../example_api_response.json';

it('renders <ColourDataStream /> with data', () => {
  const { queryAllByTestId } = render(
    <ColourDataBands fuels={testData.data.generationmix} />
  );
  expect(queryAllByTestId('components/ColourDataStream').length).toBe(10);
});

it('renders with colour data streams', () => {
  const { queryAllByTestId } = render(
    <ColourDataBands fuels={testData.data.generationmix} />
  );

  expect(queryAllByTestId('components/ColourDataStream').length).toBe(10);
});
