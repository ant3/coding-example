import React from 'react';
import PropTypes, { InferProps } from 'prop-types';
import ColourDataStream from '../ColourDataStream';
import { rainbowColours } from '../../antg.theme';

const colourDataBandProps = {
  fuels: PropTypes.arrayOf(
    PropTypes.shape({
      fuel: PropTypes.string,
      perc: PropTypes.number,
    })
  ).isRequired,
};

const ColourDataBands: React.FC<InferProps<typeof colourDataBandProps>> = ({
  fuels,
}: InferProps<typeof colourDataBandProps>) => (
  <div data-testid="components/ColourDataStream">
    {fuels &&
      fuels.map(({ fuel, perc }, index) => (
        <ColourDataStream
          key={fuel}
          data-testid="component/FullUnicorn@stream"
          color={Object.values(rainbowColours)[index] || 'white'}
          fuel={fuel}
          percentage={perc}
        />
      ))}
  </div>
);

ColourDataBands.propTypes = colourDataBandProps;

export default ColourDataBands;
